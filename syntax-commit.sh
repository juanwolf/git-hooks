#!/bin/sh
# Git commit-msg hook which will reject the commit if the commit message is not
# formated like :
# [feat|refactor|test|merge|docs|fix](<scope>): <subject>
# <new line>
# <body>
# <!-- Optionnal part -->
# <new line>
# <footer>
#
LINE_READ=0
COMMIT_HEADER_REGEXP="(feat|refactor|test|merge|docs|fix)\(*\)\s?: .+"
COMMIT_BLANK_LINE_REGEXP="^$"
COMMIT_BODY_REGEXP="^[A-Za-z].*$"
while read line;
do
    LINE_READ=$(($LINE_READ + 1))
    if [[ "$LINE_READ" == "1" ]]; then
        if ! [[ $line =~ $COMMIT_HEADER_REGEXP ]]; then
            echo -e "\x1B[31mVotre première ligne du message de votre commit ne respecte pas la convention de nommage énoncée lors de la formation Git. Elle devrait être de la forme : [feat|refactor|test|merge|docs|fix](<scope>): <subject>. Cordialement, l'intégrateur."
            echo -e "\x1B[31mLigne:  $line"
            exit 1;
        fi
    fi
    if [[ "$LINE_READ" == "2" ]]; then
        if ! [[ $line =~ $COMMIT_BLANK_LINE_REGEXP ]]; then
            echo -e "\x1B[31mVous avez oublié de sauter une ligne entre le body et la première ligne de votre message de commit. Bonne chance pour le prochain message. Cordialement, l'intégrateur."
            echo -e "\x1B[31mLigne: $line"
            exit 1;
        fi
    fi
    if [[ "$LINE_READ" == "3" ]]; then
        if ! [[ $line =~ $COMMIT_BODY_REGEXP ]]; then
            echo -e "\x1B[31mVous devez insérer au moins une ligne expliquant comment vous avez développé la nouvelle fonctionnalité. Cordialement, l'intégrateur."   
            echo "\x1B[31mLigne: $line"
        fi
    fi
done < $1
